# Simple dyndns/route53 gateway

You have a zone(s) managed by AWS Route53, and you want to use [ddclient](https://sourceforge.net/p/ddclient/wiki/Home/) to update A records for your hosts.

### Requirements

* Apache httpd (or something like that) with PHP
* AWS credentials (Key and Secret)
* **aws.phar** from [AWS PHP SDK](https://github.com/aws/aws-sdk-php/releases)

### Configuration

Create **config.php** from **config.php.sample**, and put your stuff there, make sure file permissions allow reading only to the web server user (like www-data).

Get the **aws.phar** and put it into your PHP **include_path**

Point Apache to the script like:
```
Alias "/nic/update" "/path/to/r53dyndns/update/index.php"
```

### Using

Sample **ddclient.conf** would be like:
```
# /etc/ddclient.conf

pid=/var/run/ddclient.pid
protocol=dyndns2
use=web
ssl=yes
syslog=yes
server=server.company.com
script=/nic/update
login=your_login
password=your_password
hostname.company.com
```

