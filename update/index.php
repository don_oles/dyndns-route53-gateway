<?php
require '../bootstrap.php';
function anwer_log_exit($text) {
    global $LOGFILE;
    echo $text;
    $logtext = date("r")." ".$_SERVER['REMOTE_ADDR']." $text\n";
    file_put_contents($LOGFILE,$logtext,FILE_APPEND);
    exit;
}
$hostname=$_GET['hostname'];
$myip=$_GET['myip'];
$zoneid = null;
if ($_SERVER['PHP_AUTH_USER']!=$LOGIN || $_SERVER['PHP_AUTH_PW']!=$PASSWORD)
    anwer_log_exit("badauth $myip $hostname");
if (!preg_match('/^\d+\.\d+\.\d+\.\d+$/',$myip))
    anwer_log_exit("badip $myip $hostname");
foreach(array_keys($HOSTZONES) as $domain) {
    if ($zoneid===null && endsWith($hostname,".${domain}")) {
        $zoneid = $HOSTZONES[$domain];
    }
}
if (!$zoneid)
    anwer_log_exit("nohost $myip $hostname");
$result = $r53Client->listResourceRecordSets(array(
    'HostedZoneId' => $zoneid,
    'StartRecordName' => $hostname,
    'StartRecordType' => 'A',
    'MaxItems' => 1
));
$resarr = $result->get('ResourceRecordSets');
$saved_ip = null;
if ($resarr[0]['Name'] == "${hostname}.") {
    $saved_ip = $resarr[0]['ResourceRecords'][0]['Value'];
}
if ($saved_ip == $myip)
    anwer_log_exit("nochg $saved_ip $hostname"); 
$result = $r53Client->changeResourceRecordSets(array(
    'HostedZoneId' => $zoneid,
    'ChangeBatch' => array(
        'Changes' => array(
            array(
                'Action' => 'UPSERT',
                'ResourceRecordSet' => array(
                    'Name' => "${hostname}.",
                    'Type' => 'A',
                    'TTL' => $TTL,
                    'ResourceRecords' => array(
                        array( 'Value' => "${myip}" ),
                    ),
                ),
            ),
        ),
    ),
));
$resarr = $result->get('ChangeInfo');
if ($resarr['Status']!='PENDING')
    anwer_log_exit("dnserr $myip $hostname " . $resarr['Status']);
anwer_log_exit("good $myip $hostname");
