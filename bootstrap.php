<?php
require 'aws.phar';
use Aws\Route53\Route53Client;

$TTL=300;
$LOGFILE='/var/log/r53dyndns.log';
require('config.php');

$r53Client = Route53Client::factory($AWSCONNECT);

function endsWith($haystack, $needle)
{
    $length = strlen($needle);

    return $length === 0 ||
    (substr($haystack, -$length) === $needle);
}
